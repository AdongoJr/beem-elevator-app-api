import { Inject, Injectable } from '@nestjs/common';
import { RedisClientCategory } from './constants/redis';
import { RedisClient } from './redis.provider';
import { Observable, Observer, filter, map } from 'rxjs';
import { ClientProxy } from '@nestjs/microservices';

export interface RedisSubscriberMessage {
  readonly message: string;
  readonly channel: string;
}

@Injectable()
export class RedisService {
  constructor(
    @Inject(RedisClientCategory.REDIS_SUBSCRIBER_CLIENT)
    private readonly redisSubsciberClient: RedisClient,
    @Inject(RedisClientCategory.REDIS_PUBLISHER_CLIENT)
    private readonly redisPublisherClient: ClientProxy,
  ) {}

  fromEvent<T>(eventName: string): Observable<T> {
    this.redisSubsciberClient.subscribe(eventName);

    return new Observable((observer: Observer<RedisSubscriberMessage>) =>
      this.redisSubsciberClient.on('message', (channel, message) =>
        observer.next({ channel, message }),
      ),
    ).pipe(
      filter(({ channel }) => channel === eventName),
      map(({ message }) => JSON.parse(message)),
    );
  }

  publish<T>(eventName: string, data: T) {
    return this.redisPublisherClient.emit<string, T>(eventName, data);
  }
}
