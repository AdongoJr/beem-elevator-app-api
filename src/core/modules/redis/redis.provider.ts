import { Provider } from '@nestjs/common';
import * as Redis from 'ioredis';
import { RedisClientCategory } from './constants/redis';
import { ConfigService } from '@nestjs/config';
import {
  ClientProxy,
  ClientProxyFactory,
  Transport,
} from '@nestjs/microservices';

export type RedisClient = Redis.Redis;

export const redisProviders: Array<Provider> = [
  // subscriber
  {
    provide: RedisClientCategory.REDIS_SUBSCRIBER_CLIENT,
    useFactory: (configService: ConfigService): RedisClient => {
      return new Redis.Redis({
        host: configService.get('REDIS_HOST'),
        port: configService.get('REDIS_PORT'),
      });
    },
    inject: [ConfigService],
  },
  // publisher
  {
    provide: RedisClientCategory.REDIS_PUBLISHER_CLIENT,
    useFactory: (configService: ConfigService): ClientProxy => {
      return ClientProxyFactory.create({
        transport: Transport.REDIS,
        options: {
          host: configService.get('REDIS_HOST'),
          port: configService.get('REDIS_PORT'),
          retryAttempts: 1,
          retryDelay: 2000,
        },
      });
    },
    inject: [ConfigService],
  },
];
