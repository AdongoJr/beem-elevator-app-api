import {
  MessageBody,
  OnGatewayConnection,
  SubscribeMessage,
  WebSocketGateway,
  WebSocketServer,
  WsResponse,
} from '@nestjs/websockets';
import { Logger } from '@nestjs/common';
import { Server } from 'ws';
import { Observable, merge } from 'rxjs';
import { RedisService } from '../redis/redis.service';

@WebSocketGateway(3030)
export class EventsGateway implements OnGatewayConnection {
  constructor(
    private logger: Logger,
    private redisService: RedisService,
  ) {}

  handleConnection() {
    this.logger.log('**NEW CLIENT CONNECTED**', 'ON WS CONNECT');
    this.logger.log(this.server.clients.size, 'TOTAL WS CLIENTS');
  }

  @WebSocketServer()
  server: Server;

  @SubscribeMessage('elevator-events')
  async streamElevatorEvents(
    @MessageBody() data: any,
  ): Promise<Observable<WsResponse<any>>> {
    this.logger.log(data, 'INCOMING DATA');

    const elevatorMovement$ =
      this.redisService.fromEvent<WsResponse<any>>('elevator-movement');
    const doorsOpen$ = this.redisService.fromEvent<WsResponse<any>>(
      'elevator-doors-open',
    );
    const doorsClose$ = this.redisService.fromEvent<WsResponse<any>>(
      'elevator-doors-close',
    );

    return merge(elevatorMovement$, doorsOpen$, doorsClose$);
  }
}
