import { Module } from '@nestjs/common';
import { SqlTrackerService } from './sql-tracker.service';
import { SqlTrackerController } from './sql-tracker.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { SqlTracker } from './entities/sql-tracker.entity';

@Module({
  imports: [TypeOrmModule.forFeature([SqlTracker])],
  controllers: [SqlTrackerController],
  providers: [SqlTrackerService],
  exports: [SqlTrackerService, TypeOrmModule],
})
export class SqlTrackerModule {}
