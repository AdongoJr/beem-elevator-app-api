import { QueryCaller } from '../enums/query-caller';

export class CreateSqlTrackerDto {
  caller: QueryCaller;
  query: string;
}
