import { PartialType } from '@nestjs/mapped-types';
import { CreateSqlTrackerDto } from './create-sql-tracker.dto';

export class UpdateSqlTrackerDto extends PartialType(CreateSqlTrackerDto) {}
