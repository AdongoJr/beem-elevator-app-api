import { Test, TestingModule } from '@nestjs/testing';
import { SqlTrackerController } from './sql-tracker.controller';
import { SqlTrackerService } from './sql-tracker.service';

describe('SqlTrackerController', () => {
  let controller: SqlTrackerController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [SqlTrackerController],
      providers: [SqlTrackerService],
    }).compile();

    controller = module.get<SqlTrackerController>(SqlTrackerController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
