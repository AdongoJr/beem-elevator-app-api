import { Column, Entity } from 'typeorm';
import { CommonEntityFields } from '../../../utils/entities/CommonEntityFields';
import { QueryCaller } from '../enums/query-caller';

@Entity()
export class SqlTracker extends CommonEntityFields {
  constructor(partial: Partial<SqlTracker>) {
    super();
    Object.assign(this, partial);
  }

  @Column({
    type: 'enum',
    enum: QueryCaller,
    nullable: true,
    default: QueryCaller.ELEVATOR,
  })
  caller: QueryCaller;

  @Column({
    length: 1024,
    nullable: true,
  })
  query: string;
}
