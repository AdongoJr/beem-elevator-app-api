import { Test, TestingModule } from '@nestjs/testing';
import { SqlTrackerService } from './sql-tracker.service';

describe('SqlTrackerService', () => {
  let service: SqlTrackerService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [SqlTrackerService],
    }).compile();

    service = module.get<SqlTrackerService>(SqlTrackerService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
