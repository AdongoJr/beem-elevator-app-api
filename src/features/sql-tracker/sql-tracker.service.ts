import { Injectable } from '@nestjs/common';
import { CreateSqlTrackerDto } from './dto/create-sql-tracker.dto';
import { SqlTracker } from './entities/sql-tracker.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

@Injectable()
export class SqlTrackerService {
  constructor(
    @InjectRepository(SqlTracker)
    private elevatorEventLogRepository: Repository<SqlTracker>,
  ) {}

  async create(createSqlTrackerDto: CreateSqlTrackerDto) {
    try {
      const newRecord =
        this.elevatorEventLogRepository.create(createSqlTrackerDto);
      const savedRecord = await this.elevatorEventLogRepository.save(newRecord);
      return new SqlTracker({ ...savedRecord });
    } catch (error) {
      throw error;
    }
  }
}
