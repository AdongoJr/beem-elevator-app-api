import { Module, forwardRef } from '@nestjs/common';
import { ElevatorService } from './elevator.service';
import { ElevatorController } from './elevator.controller';
import { ApiResponseModule } from 'src/core/modules/api-response/api-response.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Elevator } from './entities/elevator.entity';
import { RedisModule } from 'src/core/modules/redis/redis.module';
import { EventsModule } from 'src/core/modules/events/events.module';
import { ElevatorEventLogModule } from '../elevator-event-log/elevator-event-log.module';
import { SqlTrackerModule } from '../sql-tracker/sql-tracker.module';

@Module({
  imports: [
    TypeOrmModule.forFeature([Elevator]),
    ApiResponseModule,
    RedisModule,
    SqlTrackerModule,
    forwardRef(() => EventsModule),
    ElevatorEventLogModule,
  ],
  controllers: [ElevatorController],
  providers: [ElevatorService],
  exports: [TypeOrmModule, ElevatorService],
})
export class ElevatorModule {}
