import { Injectable, Logger } from '@nestjs/common';
import { CreateElevatorDto } from './dto/create-elevator.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Brackets, Repository } from 'typeorm';
import { Elevator } from './entities/elevator.entity';
import { CallElevatorDto } from './dto/call-elevator.dto';
import { NotFoundException } from 'src/utils/exceptions/not-found.exception';
import { ElevatorDirection } from './enums/elevator-direction';
import { RedisService } from 'src/core/modules/redis/redis.service';
import { ElevatorEventLogService } from '../elevator-event-log/elevator-event-log.service';
import { SqlTrackerService } from '../sql-tracker/sql-tracker.service';
import { QueryCaller } from '../sql-tracker/enums/query-caller';
import { getCompleteSqlQuery } from 'src/utils/transformations/sql-query';

@Injectable()
export class ElevatorService {
  constructor(
    @InjectRepository(Elevator)
    private elevatorRepository: Repository<Elevator>,
    private logger: Logger,
    private redisService: RedisService,
    private elevatorEventLogService: ElevatorEventLogService,
    private sqlTrackerService: SqlTrackerService,
  ) {}

  private readonly elevatorAlias = 'elevator';

  async create(createElevatorDto: CreateElevatorDto) {
    try {
      const queryBuilder = this.elevatorRepository
        .createQueryBuilder(this.elevatorAlias)
        .insert()
        .into(Elevator)
        .values(createElevatorDto);

      const [sql, params] = queryBuilder.getQueryAndParameters();
      const sqlQuery = getCompleteSqlQuery(sql, params);

      this.sqlTrackerService.create({
        caller: QueryCaller.USER,
        query: sqlQuery,
      });

      const savedRecord = await queryBuilder.execute();
      return savedRecord.identifiers;
    } catch (error) {
      throw error;
    }
  }

  async findAll() {
    try {
      const queryBuilder = this.elevatorRepository.createQueryBuilder(
        this.elevatorAlias,
      );

      const [sql, params] = queryBuilder.getQueryAndParameters();
      const sqlQuery = getCompleteSqlQuery(sql, params);

      this.sqlTrackerService.create({
        caller: QueryCaller.USER,
        query: sqlQuery,
      });

      return queryBuilder.getManyAndCount();
    } catch (error) {
      throw error;
    }
  }

  async findOne(id: number, caller: QueryCaller = QueryCaller.USER) {
    try {
      const queryBuilder = this.elevatorRepository.createQueryBuilder(
        this.elevatorAlias,
      );

      const sqlQueryBuilder = queryBuilder.andWhere(
        `${this.elevatorAlias}.id = :id`,
        {
          id,
        },
      );

      const [sql, params] = sqlQueryBuilder.getQueryAndParameters();
      const sqlQuery = getCompleteSqlQuery(sql, params);

      this.sqlTrackerService.create({
        caller,
        query: sqlQuery,
      });

      const foundItem = sqlQueryBuilder.getOne();

      if (!foundItem) {
        throw new NotFoundException(`Elevator with id: ${id} does not exist`);
      }

      return foundItem;
    } catch (error) {
      throw error;
    }
  }

  async call(callElevatorDto: CallElevatorDto) {
    try {
      const queryBuilder = this.elevatorRepository.createQueryBuilder(
        this.elevatorAlias,
      );

      const sqlQueryBuilder = queryBuilder.orWhere(
        new Brackets((qb) => {
          qb.orWhere(`${this.elevatorAlias}.direction IS NULL`);
          qb.orWhere(`${this.elevatorAlias}.direction = :direction`, {
            direction: ElevatorDirection.STATIONARY,
          });
        }),
      );

      const [sql, params] = sqlQueryBuilder.getQueryAndParameters();
      const sqlQuery = getCompleteSqlQuery(sql, params);

      this.sqlTrackerService.create({
        caller: QueryCaller.USER,
        query: sqlQuery,
      });

      const foundElevator = await sqlQueryBuilder.getOne();

      if (!foundElevator) {
        throw new NotFoundException('No elevator found');
      }

      const updatedElevator = await this.updateElevator(foundElevator.id, {
        destinationFloor: callElevatorDto.destinationFloor,
      });

      this.trackElevatorMovement(updatedElevator);

      return updatedElevator;
    } catch (error) {
      throw error;
    }
  }

  private async trackElevatorMovement(elevator: Elevator) {
    let updatedElevator: Elevator | null = null;

    const floorsDifference =
      (elevator.destinationFloor ?? 0) - (elevator.currentFloor ?? 0);

    if (floorsDifference > 0) {
      // moving up
      updatedElevator = await this.updateElevator(elevator.id, {
        direction: ElevatorDirection.UP,
      });
    } else if (floorsDifference < 0) {
      // moving down
      updatedElevator = await this.updateElevator(elevator.id, {
        direction: ElevatorDirection.DOWN,
      });
    } else {
      // stationary
      updatedElevator = await this.updateElevator(elevator.id, {
        direction: ElevatorDirection.STATIONARY,
      });
    }

    this.logger.log(
      `Elevator initial floor: ${updatedElevator.currentFloor ?? 0}`,
      `ELEVATOR ${updatedElevator.id} MOVEMENT`,
    );
    this.logger.log(
      `Elevator destination floor: ${updatedElevator.destinationFloor}`,
      `ELEVATOR ${updatedElevator.id} MOVEMENT`,
    );

    this.logger.log(
      `Elevator direction: ${updatedElevator.direction}`,
      `ELEVATOR ${updatedElevator.id} MOVEMENT`,
    );

    this.logElevatorEventsToDB(updatedElevator, 'no action');

    this.redisService.publish(`elevator-movement`, {
      elevatorId: updatedElevator.id,
      direction: `${updatedElevator.direction}`,
      currentFloor: `${updatedElevator.currentFloor ?? 0}`,
      destinationFloor: `${updatedElevator.destinationFloor}`,
    });

    let count = updatedElevator.currentFloor ?? 0;

    if (count === updatedElevator.destinationFloor) {
      await this.openElevatorDoors(updatedElevator);
      return;
    }

    const intervalFunction = async () => {
      if (updatedElevator.direction === ElevatorDirection.UP) {
        count++;
      } else {
        count--;
      }

      updatedElevator = await this.updateElevator(elevator.id, {
        currentFloor: count,
      });

      this.logger.log(
        `Count: ${count}`,
        `ELEVATOR ${updatedElevator.id} MOVEMENT`,
      );
      this.logger.log(
        `Elevator current floor: ${updatedElevator.currentFloor ?? 0}`,
        `ELEVATOR ${updatedElevator.id} MOVEMENT`,
      );

      this.logElevatorEventsToDB(updatedElevator, 'no action');

      this.redisService.publish(`elevator-movement`, {
        elevatorId: updatedElevator.id,
        direction: `${updatedElevator.direction}`,
        currentFloor: `${updatedElevator.currentFloor ?? 0}`,
        destinationFloor: `${updatedElevator.destinationFloor}`,
      });

      if (count === updatedElevator.destinationFloor) {
        updatedElevator = await this.updateElevator(elevator.id, {
          direction: ElevatorDirection.STATIONARY,
        });

        await this.openElevatorDoors(updatedElevator);
        clearInterval(intervalId);
      }
    };

    const intervalId: NodeJS.Timeout = setInterval(intervalFunction, 5000);
  }

  private async openElevatorDoors(updatedElevator: Elevator) {
    this.logger.log(
      'ELEVATOR DOORS OPENING',
      `ELEVATOR ${updatedElevator.id} DOORS`,
    );

    this.logElevatorEventsToDB(updatedElevator, 'opening');

    this.redisService.publish(`elevator-doors-open`, {
      elevatorId: updatedElevator.id,
      direction: `${updatedElevator.direction}`,
      currentFloor: `${updatedElevator.currentFloor ?? 0}`,
      destinationFloor: `${updatedElevator.destinationFloor}`,
    });

    const intervalId: NodeJS.Timeout = setInterval(() => {
      this.logger.log(
        'ELEVATOR DOORS CLOSED',
        `ELEVATOR ${updatedElevator.id} DOORS`,
      );

      this.logElevatorEventsToDB(updatedElevator, 'closing');

      this.redisService.publish(`elevator-doors-close`, {
        elevatorId: updatedElevator.id,
        direction: `${updatedElevator.direction}`,
        currentFloor: `${updatedElevator.currentFloor ?? 0}`,
        destinationFloor: `${updatedElevator.destinationFloor}`,
      });

      clearInterval(intervalId);
    }, 2000);
  }

  private async updateElevator(
    elevatorId: number,
    elevatorDetails: Partial<Elevator>,
  ) {
    try {
      const foundRecord = await this.findOne(elevatorId, QueryCaller.ELEVATOR);

      const queryBuilder = this.elevatorRepository
        .createQueryBuilder(this.elevatorAlias)
        .update(Elevator)
        .set({
          ...foundRecord,
          ...elevatorDetails,
        })
        .where('id = :id', { id: foundRecord.id });

      const [sql, params] = queryBuilder.getQueryAndParameters();
      const sqlQuery = getCompleteSqlQuery(sql, params);

      this.sqlTrackerService.create({
        caller: QueryCaller.ELEVATOR,
        query: sqlQuery,
      });

      await queryBuilder.execute();

      const updatedElevator = await this.findOne(
        elevatorId,
        QueryCaller.ELEVATOR,
      );
      return updatedElevator;
    } catch (error) {
      throw error;
    }
  }

  private logElevatorEventsToDB(
    elevator: Elevator,
    doorsAction: 'opening' | 'closing' | 'no action',
  ) {
    let doorsActionText = '';
    if (doorsAction === 'closing') {
      doorsActionText = 'Doors closing';
    } else if (doorsAction === 'opening') {
      doorsActionText = 'Doors opening';
    }

    this.elevatorEventLogService.create(
      {
        log: `${
          elevator.currentFloor !== elevator.destinationFloor
            ? `Moving ${elevator.direction} towards floor ${
                elevator.destinationFloor
              }. Current floor is ${elevator.currentFloor ?? 0}`
            : `Stationary at floor ${elevator.currentFloor}. ${doorsActionText}`
        }`,
      },
      elevator,
    );
  }
}
