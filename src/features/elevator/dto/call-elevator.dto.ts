import { IsDefined, IsNotEmpty } from 'class-validator';

export class CallElevatorDto {
  @IsDefined({
    message: '$property is required',
  })
  @IsNotEmpty({
    message: '$property cannot be empty',
  })
  destinationFloor: number;
}
