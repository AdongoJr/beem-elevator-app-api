import { Column, Entity, OneToMany } from 'typeorm';
import { CommonEntityFields } from '../../../utils/entities/CommonEntityFields';
import { ElevatorDirection } from '../enums/elevator-direction';
import { ElevatorEventLog } from '../../../features/elevator-event-log/entities/elevator-event-log.entity';

@Entity()
export class Elevator extends CommonEntityFields {
  constructor(partial: Partial<Elevator>) {
    super();
    Object.assign(this, partial);
  }

  @Column({
    type: 'enum',
    enum: ElevatorDirection,
    nullable: true,
  })
  direction: ElevatorDirection;

  @Column({
    type: 'int',
    nullable: true,
  })
  currentFloor: number;

  @Column({
    type: 'int',
    nullable: true,
  })
  destinationFloor: number;

  @OneToMany(() => ElevatorEventLog, (eventLog) => eventLog.elevator)
  eventLogs: ElevatorEventLog[];
}
