import {
  Controller,
  Get,
  Post,
  Body,
  Param,
  HttpCode,
  HttpStatus,
  InternalServerErrorException,
  ParseIntPipe,
} from '@nestjs/common';
import { ElevatorService } from './elevator.service';
import { CreateElevatorDto } from './dto/create-elevator.dto';
import { DatabaseErrorException } from 'src/utils/exceptions/database-error.exception';
import { formatDatabaseError } from 'src/utils/transformations/database-errors';
import { ApiResponseService } from 'src/core/modules/api-response/api-response/api-response.service';
import { DtoValidationPipe } from 'src/core/pipes/dto-validation/dto-validation.pipe';
import { CallElevatorDto } from './dto/call-elevator.dto';
import { ApiListResponseService } from 'src/core/modules/api-response/api-list-response/api-list-response.service';
import { NotFoundException } from 'src/utils/exceptions/not-found.exception';

@Controller('elevators')
export class ElevatorController {
  constructor(
    private readonly elevatorService: ElevatorService,
    private readonly apiResponseService: ApiResponseService,
    private readonly apiListResponseService: ApiListResponseService,
  ) {}

  @HttpCode(HttpStatus.OK)
  @Post()
  async create(@Body() createElevatorDto: CreateElevatorDto) {
    try {
      const createdElevator =
        await this.elevatorService.create(createElevatorDto);

      const responseBody = this.apiResponseService.getResponseBody({
        status: 'success',
        message: 'Elevator created successfully',
        data: createdElevator,
      });

      return responseBody;
    } catch (error) {
      // errors from the database
      if (error?.driverError) {
        const { message } = formatDatabaseError(error.driverError);
        throw new DatabaseErrorException(message);
      }

      // other errors
      throw new InternalServerErrorException(error);
    }
  }

  @Get()
  async findAll() {
    try {
      const [records, count] = await this.elevatorService.findAll();

      const responseBody = this.apiListResponseService.getResponseBody({
        message: 'Elevators retrieved successfully',
        count,
        pageSize: 1000,
        records,
      });

      return responseBody;
    } catch (error) {
      // other errors
      throw new InternalServerErrorException(error);
    }
  }

  @Get(':id')
  async findOne(@Param('id', ParseIntPipe) id: number) {
    try {
      const foundRecord = await this.elevatorService.findOne(id);

      const responseBody = this.apiResponseService.getResponseBody({
        status: 'success',
        message: 'Elevator retrieved successfully',
        data: foundRecord,
      });

      return responseBody;
    } catch (error) {
      if (error?.name === 'NotFoundException') {
        throw new NotFoundException(error?.message);
      }

      // other errors
      throw new InternalServerErrorException(error);
    }
  }

  @HttpCode(HttpStatus.OK)
  @Post('call')
  async call(@Body(new DtoValidationPipe()) callElevatorDto: CallElevatorDto) {
    try {
      const foundElevator = await this.elevatorService.call(callElevatorDto);

      const responseBody = this.apiResponseService.getResponseBody({
        status: 'success',
        message: 'Elevator requested successfully',
        data: foundElevator,
      });

      return responseBody;
    } catch (error) {
      // errors from the database
      if (error?.driverError) {
        const { message } = formatDatabaseError(error.driverError);
        throw new DatabaseErrorException(message);
      }

      if (error?.name === 'NotFoundException') {
        throw new NotFoundException(error?.message);
      }

      // other errors
      throw new InternalServerErrorException(error);
    }
  }
}
