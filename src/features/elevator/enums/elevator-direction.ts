export enum ElevatorDirection {
  UP = 'UP',
  DOWN = 'DOWN',
  STATIONARY = 'STATIONARY',
}
