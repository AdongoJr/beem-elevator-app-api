import { Module } from '@nestjs/common';
import { ElevatorEventLogService } from './elevator-event-log.service';
import { ElevatorEventLogController } from './elevator-event-log.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ElevatorEventLog } from './entities/elevator-event-log.entity';
import { SqlTrackerModule } from '../sql-tracker/sql-tracker.module';

@Module({
  imports: [TypeOrmModule.forFeature([ElevatorEventLog]), SqlTrackerModule],
  controllers: [ElevatorEventLogController],
  providers: [ElevatorEventLogService],
  exports: [ElevatorEventLogService, TypeOrmModule],
})
export class ElevatorEventLogModule {}
