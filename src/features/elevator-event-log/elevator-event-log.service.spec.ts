import { Test, TestingModule } from '@nestjs/testing';
import { ElevatorEventLogService } from './elevator-event-log.service';

describe('ElevatorEventLogService', () => {
  let service: ElevatorEventLogService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [ElevatorEventLogService],
    }).compile();

    service = module.get<ElevatorEventLogService>(ElevatorEventLogService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
