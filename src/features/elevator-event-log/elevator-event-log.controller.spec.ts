import { Test, TestingModule } from '@nestjs/testing';
import { ElevatorEventLogController } from './elevator-event-log.controller';
import { ElevatorEventLogService } from './elevator-event-log.service';

describe('ElevatorEventLogController', () => {
  let controller: ElevatorEventLogController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [ElevatorEventLogController],
      providers: [ElevatorEventLogService],
    }).compile();

    controller = module.get<ElevatorEventLogController>(
      ElevatorEventLogController,
    );
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
