import { Injectable } from '@nestjs/common';
import { CreateElevatorEventLogDto } from './dto/create-elevator-event-log.dto';
import { ElevatorEventLog } from './entities/elevator-event-log.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Elevator } from '../elevator/entities/elevator.entity';
import { getCompleteSqlQuery } from 'src/utils/transformations/sql-query';
import { SqlTrackerService } from '../sql-tracker/sql-tracker.service';
import { QueryCaller } from '../sql-tracker/enums/query-caller';

@Injectable()
export class ElevatorEventLogService {
  constructor(
    @InjectRepository(ElevatorEventLog)
    private elevatorEventLogRepository: Repository<ElevatorEventLog>,
    private sqlTrackerService: SqlTrackerService,
  ) {}

  private readonly elevatorEventLogAlias = 'elevator_event_log';

  async create(
    createElevatorEventLogDto: CreateElevatorEventLogDto,
    elevator: Elevator,
  ) {
    try {
      const queryBuilder = this.elevatorEventLogRepository
        .createQueryBuilder(this.elevatorEventLogAlias)
        .insert()
        .into(ElevatorEventLog)
        .values({
          ...createElevatorEventLogDto,
          elevator,
        });

      const [sql, params] = queryBuilder.getQueryAndParameters();
      const sqlQuery = getCompleteSqlQuery(sql, params);

      this.sqlTrackerService.create({
        caller: QueryCaller.ELEVATOR,
        query: sqlQuery,
      });

      const savedRecord = await queryBuilder.execute();
      return savedRecord.identifiers;
    } catch (error) {
      throw error;
    }
  }
}
