import { Column, Entity, JoinColumn, ManyToOne } from 'typeorm';
import { CommonEntityFields } from '../../../utils/entities/CommonEntityFields';
import { Elevator } from '../../../features/elevator/entities/elevator.entity';

@Entity()
export class ElevatorEventLog extends CommonEntityFields {
  constructor(partial: Partial<ElevatorEventLog>) {
    super();
    Object.assign(this, partial);
  }

  @ManyToOne(() => Elevator, (elevator) => elevator.eventLogs, {
    onUpdate: 'CASCADE',
    onDelete: 'RESTRICT',
  })
  @JoinColumn()
  elevator: Elevator;

  @Column({
    length: 256,
    nullable: true,
  })
  log: string;
}
