import { PartialType } from '@nestjs/mapped-types';
import { CreateElevatorEventLogDto } from './create-elevator-event-log.dto';

export class UpdateElevatorEventLogDto extends PartialType(
  CreateElevatorEventLogDto,
) {}
