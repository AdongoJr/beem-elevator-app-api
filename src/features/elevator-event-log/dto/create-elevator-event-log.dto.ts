import { IsDefined, IsNotEmpty } from 'class-validator';

export class CreateElevatorEventLogDto {
  @IsDefined({
    message: '$property is required',
  })
  @IsNotEmpty({
    message: '$property cannot be empty',
  })
  log: string;
}
