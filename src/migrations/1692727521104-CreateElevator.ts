import { MigrationInterface, QueryRunner } from 'typeorm';

export class CreateElevator1692727521104 implements MigrationInterface {
  name = 'CreateElevator1692727521104';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`
            CREATE TYPE "public"."elevator_direction_enum" AS ENUM('UP', 'DOWN', 'STATIONARY')
        `);
    await queryRunner.query(`
            CREATE TABLE "elevator" (
                "id" SERIAL NOT NULL,
                "createdAt" TIMESTAMP NOT NULL DEFAULT now(),
                "updatedAt" TIMESTAMP NOT NULL DEFAULT now(),
                "direction" "public"."elevator_direction_enum",
                "currentFloor" integer,
                "destinationFloor" integer,
                CONSTRAINT "PK_7be0c527c8681d7478de2e89efb" PRIMARY KEY ("id")
            )
        `);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`
            DROP TABLE "elevator"
        `);
    await queryRunner.query(`
            DROP TYPE "public"."elevator_direction_enum"
        `);
  }
}
