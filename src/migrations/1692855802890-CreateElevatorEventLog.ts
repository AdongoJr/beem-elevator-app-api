import { MigrationInterface, QueryRunner } from 'typeorm';

export class CreateElevatorEventLog1692855802890 implements MigrationInterface {
  name = 'CreateElevatorEventLog1692855802890';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`
            CREATE TABLE "elevator_event_log" (
                "id" SERIAL NOT NULL,
                "createdAt" TIMESTAMP NOT NULL DEFAULT now(),
                "updatedAt" TIMESTAMP NOT NULL DEFAULT now(),
                "log" character varying(256),
                "elevatorId" integer,
                CONSTRAINT "PK_f860dde50ad80eb0652188ae825" PRIMARY KEY ("id")
            )
        `);
    await queryRunner.query(`
            ALTER TABLE "elevator_event_log"
            ADD CONSTRAINT "FK_e6c91bb9857e6a76c6dae65fdf4" FOREIGN KEY ("elevatorId") REFERENCES "elevator"("id") ON DELETE RESTRICT ON UPDATE CASCADE
        `);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`
            ALTER TABLE "elevator_event_log" DROP CONSTRAINT "FK_e6c91bb9857e6a76c6dae65fdf4"
        `);
    await queryRunner.query(`
            DROP TABLE "elevator_event_log"
        `);
  }
}
