import { MigrationInterface, QueryRunner } from 'typeorm';

export class CreateSQLTracker1692873406635 implements MigrationInterface {
  name = 'CreateSQLTracker1692873406635';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`
            CREATE TYPE "public"."sql_tracker_caller_enum" AS ENUM('USER', 'ELEVATOR')
        `);
    await queryRunner.query(`
            CREATE TABLE "sql_tracker" (
                "id" SERIAL NOT NULL,
                "createdAt" TIMESTAMP NOT NULL DEFAULT now(),
                "updatedAt" TIMESTAMP NOT NULL DEFAULT now(),
                "caller" "public"."sql_tracker_caller_enum" DEFAULT 'ELEVATOR',
                "query" character varying(1024),
                CONSTRAINT "PK_f3704dc04b09dea328dd3fb7bd6" PRIMARY KEY ("id")
            )
        `);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`
            DROP TABLE "sql_tracker"
        `);
    await queryRunner.query(`
            DROP TYPE "public"."sql_tracker_caller_enum"
        `);
  }
}
