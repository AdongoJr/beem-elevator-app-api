export const getCompleteSqlQuery = (
  sql: string,
  params: Array<any>,
): string => {
  let result = sql;

  params.forEach((value, index) => {
    if (typeof value === 'string') {
      result = result.replace(`$${index + 1}`, `'${value}'`);
    }
    if (typeof value === 'object') {
      if (Array.isArray(value)) {
        result = result.replace(
          `$${index + 1}`,
          value
            .map((element) =>
              typeof element === 'string' ? `'${element}'` : element,
            )
            .join(','),
        );
      } else {
        result = result.replace(`$${index + 1}`, value);
      }
    }

    if (['number', 'boolean'].includes(typeof value)) {
      result = result.replace(`$${index + 1}`, value.toString());
    }
  });

  return result;
};
