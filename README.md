# BEEM Elevator API
## Description

Beem Elevator API based on the description on 
<a href="https://docs.google.com/document/d/1vDxmmSSLfmWl593FTVmmSNTULsrspYKIRJMtYaXl9w0/edit#heading=h.6mzvhdayhp" target="_blank">this document</a>

## Tech Stack
* Server framework - [NestJS](https://nestjs.com/) + [TypeORM](https://typeorm.io/)
* Database - [PostgreSQL](https://www.postgresql.org/)
* Real-time messaging - [Redis PubSub](https://redis.io/docs/interact/pubsub/) + Websockets

## Getting Started
### Requirements
* <a href="https://docs.docker.com/compose/" target="_blank">Docker Compose</a> - for running the docker containerized applications
* <a href="https://www.postman.com/" target="_blank">Postman</a> (or any other application for consuming REST and Websocket API endpoints)

### Installation and Setup
* Clone this
<a href="https://gitlab.com/AdongoJr/beem-elevator-app-api.git" target="_blank">repository</a> 
to your machine
* Create `.env` file at the root of your project and populate it using the template below.
(NOTE: use your own values instead of the values with square brackets). A sample can be found in [.env.example](./.env.example) file a the root of this project.

```
NODE_ENV=development

PORT=[your-development-server-port-number]
WS_PORT=[your-development-web-socket-server-port-number]

DB_HOST=postgres
DB_PORT=5432
DB_NAME=[your-db-name]
DB_USERNAME=[your-db-username]
DB_PASSWORD=[your-db-password]

PGADMIN_DEFAULT_EMAIL=[your-pg-admin-default-email]
PGADMIN_DEFAULT_PASSWORD=[your-pg-admin-default-password]

REDIS_HOST=[your-redis-host]
REDIS_PORT=[your-redis-port]
```

### Running the Application
- Use the command below to spin up the docker containers

```bash
# development (runs the server in watch mode)
$ docker-compose up -d --build
```

- Inspect the logs inside the web server container and wait until the last log reads:
`Server Listening on PORT: [your-set-server-port]`
- Run database migrations using the command below (from the server container terminal) to create the database tables
```bash
$ yarn migration:run
```
- From Postman, make a `POST` request to `http://127.0.0.3005/api/v1/elevators/` to add an elevator to the database (You can add as many/few as you want)

#### Websocket setup
- On a new Postman tab, create a new Websocket connection to `ws://127.0.0.1:3030/`
- Make a request to `ws://127.0.0.1:3030/` with the payload below to subscribe to real-time elevator events. Keep this tab open to see real-time elevator events after calling an elevator (next step)
```json
{"event": "elevator-events","data": {}}
```
#### Calling an Elevator
- On a new Postman tab, make a `POST` request to `http://127.0.0.3005/api/v1` with the payload below to call an elevator. You can change the `destinationFloor` value to call an elevator to a different floor.
```json
{
    "destinationFloor": 3
}
```
- Switch to your Postman Websockets tab to see real-time events about the called elevator(s) state and movements.

### Running Tests
Inside the container, use the command(s) below to run the various tests 
```bash
# unit tests
$ yarn run test

# e2e tests
$ yarn run test:e2e

# test coverage
$ yarn run test:cov
```
### Shutting down containers
```bash
# shutting down the app
$ docker-compose down
```
## Incomplete Tasks 👷🏾‍♂️⚒️
- Writing tests 🧪

<!-- ## License

Nest is [MIT licensed](LICENSE). -->
## Contributors

- Author - <a href="https://gitlab.com/AdongoJr" target="_blank">Moses Adongo</a>

